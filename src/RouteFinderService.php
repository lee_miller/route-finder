<?php

namespace leemiller\RouteFinder;

class RouteFinderService
{
    /**
     * @param Route $route
     * @return Route
     */
    public function optimizeRoute(Route $route)
    {
        $waypoints = $route->waypoints;

        if (count($waypoints) < 2) {
            throw new \Exception('Less than 2 way points');
        }
        elseif (count($waypoints) <= 3 ) {
            return clone $route;
        }

        $start = $waypoints[0];
        $end = $waypoints[count($waypoints)-1];
        $otherWaypoints = array_slice($waypoints, 1, count($waypoints)-2);

        $generator = $this->permutations($otherWaypoints);

        $fastestRoute = null;
        $fastestRouteDistance = null;

        foreach($generator as $permutation) {

            $possibleWaypoints = array_merge([$start],$permutation,[$end]);
            $possibleRoute = new Route($possibleWaypoints);
            $possibleRouteDistance = $possibleRoute->calculateTotalDistance();

            if ($fastestRoute == null || $possibleRouteDistance < $fastestRouteDistance ) {
                $fastestRoute = $possibleRoute;
                $fastestRouteDistance = $possibleRouteDistance;
            }
        }

        return $fastestRoute;
    }

    private function permutations(array $elements)
    {
		// Source: https://stackoverflow.com/questions/5506888/permutations-all-possible-sets-of-numbers
        if (count($elements) <= 1) {
            yield $elements;
        } else {
            foreach ($this->permutations(array_slice($elements, 1)) as $permutation) {
                foreach (range(0, count($elements) - 1) as $i) {
                    yield array_merge(
                        array_slice($permutation, 0, $i),
                        [$elements[0]],
                        array_slice($permutation, $i)
                    );
                }
            }
        }
    }
}


