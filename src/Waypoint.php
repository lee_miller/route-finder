<?php

namespace leemiller\RouteFinder;

class Waypoint
{
    public $long;
    public $lat;
    public $name;

    public function __construct($lat, $long, $name)
    {
        $this->lat = $lat;
        $this->long = $long;
        $this->name = $name;
    }

    /**
     * @param Waypoint $other
     * @return float
     */
    public function distanceTo(Waypoint $other)
    {
        return $this->getDistanceBetweenPoints($this->lat, $this->long, $other->lat, $other->long, 'kilometers');
    }

    /**
     * @param $latitude1
     * @param $longitude1
     * @param $latitude2
     * @param $longitude2
     * @param $unit
     * @return float
     */
    private function getDistanceBetweenPoints($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'miles') {
		
		// Source: https://martech.zone/calculate-great-circle-distance/
		
        $theta = $longitude1 - $longitude2;
        $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
        $distance = acos($distance);
        $distance = rad2deg($distance);
        $distance = $distance * 60 * 1.1515;
        switch($unit) {
            case 'miles':
                break;
            case 'kilometers' :
                $distance = $distance * 1.609344;
        }
        return (round($distance,2));
    }
}

