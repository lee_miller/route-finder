<?php

namespace leemiller\RouteFinder;

class Route
{
    /**
     * @var Waypoint[]
     */
    public $waypoints = [];

    public function __construct($waypoints  = [])
    {
        $this->waypoints = $waypoints;
    }

    public function __toString()
    {
        if (!$this->waypoints || count($this->waypoints) < 2) {
            throw new \Exception ('Not enough waypoints defined for route.');
        }

        $list = [];
        foreach($this->waypoints as $waypoint) {
            $list[] = $waypoint->name ? $waypoint->name : ($waypoint->lat.','.$waypoint->long);
        }
        return join(',',$list);
    }

    public function calculateTotalDistance()
    {
        if (!$this->waypoints || count($this->waypoints) < 2) {
            throw new \Exception ('Not enough waypoints defined for route.');
        }

        $total = 0;
        $previous = $this->waypoints[0];

        foreach(array_slice($this->waypoints, 1) as $waypoint) {
            $distance = $previous->distanceTo($waypoint);
            //echo "Distance between $previous->name and $waypoint->name: $distance<BR>";
            $total += $distance;
            $previous = $waypoint;
        }

        return $total;
    }
}

